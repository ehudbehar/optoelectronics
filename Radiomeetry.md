## Radiometric definitions and units


| Quantity | Definition | SI units | Mathematical definition |
| ------ | ------ | :------: | :------: |
| Radiant energy | The amount of energy transferred in the form of electromagnetic radiation | Joules (J)| $`Q_e`$ |
| Radiant flux | The rate of flow of energy per unit time. This is sometimes called *optical power* or *radiant power*| Watts (W)| $`\Phi_e=\dfrac{\partial Q_e}{\partial t}`$|
| Radiant flux density | The radiant flux per unit area on a surface <br>There are two cases: <br>**Irradiancee** - the radiant flux incident on the surface per unit area. <br> **Radiant exitance** - the the radiant flux exiting from a unit area on the surface in all directions.<br> Radiant emittance and radiosity are older terms (no longer used) for radiant exitance| W m<sup>-2</sup>| $`E_e\text{ or } H=\dfrac{\partial \Phi_e}{\partial A}`$ |
| Radiant intensity | The radiant power emitted from a point source per unit solid angle | W sr<sup>-1</sup>| $`I_{e,\Omega}=\dfrac{\partial \Phi_e}{\partial \Omega}`$ |
| Radiance | The radiant power per unit solid angle per unit projected source area | W sr<sup>-1</sup>m<sup>-2</sup> | $`N \text{ or } L_{e,\Omega}=\dfrac{\partial^2 \Phi_e}{\partial \Omega \partial A \cos \theta}`$|

## Solid angle of a cone
```math
\Omega=2\pi(1-\cos \theta)\approx \pi \theta^2
```
![alt text](images/solidangl1e.png "Title Text")

### ƒ/N
```math
N=\frac{f}{D
```
 - If $`f=10`$ mm, $`D=5`$ mm, then the f-number would be $`2`$ and expressed as ƒ/2 in a lens system.
 - If a camera is stopped down to ƒ/8 and $`f=4`$ cm, then $`D=0.5`$ cm.

 ### Pedrotti Exercise
 A perfectly diffuse, or Lambertian, surface has the form of a square, 5 cm on a side. This object radiates a total power of 25 W into the forward directions that constitute half the total solid angle of $`4\pi`$. A camera with a 4-cm focal length lens and stopped down to f/8 is used to photograph the object when it is placed 1 m from the lens.
 1. Determine the radiant exitance, radiant intensity, and radiance of the object.
    
    **Radiant exitance**: $`H=\frac{\Phi_e}{A}=\frac{25 W}{25 cm^2}`$
    
    **Radiant intensity**: This quantity is a function of the angle $`\theta`$ from which you measure. Therefore we should write
    ```math
    I_e = I_e\left(0\right)\cos \theta\Rightarrow \Phi_e = \iint I_e \mathrm{d} \Omega=\int_0^{2\pi}\mathrm{d}\phi \int_0^{\pi/2}I_e(0)\cos\theta\sin\theta\mathrm{d}\theta=2\pi I_e(0)\frac{1}{2}=\pi I_e(0)\Rightarrow I_e(0)=\frac{\Phi_e}{\pi }=\frac{25 \text{ W}}{\pi \text{ sr}}=7.96 \text{W}/\text{sr}
    ```
    This is the radiant intensity in the direction of the surface normal to the source.
    **Radiance**: Since the source is Lambertian the radiance is independent of the viewing angle $`θ`$ and is given by
    ```math
    L_e=\frac{I(0)\cos\theta}{A\cos\theta}=\frac{I(0)}{A}=\frac{7.96 \text{W}/\text{sr}}{25 \text{cm}^2}=3180\frac{\text{W}}{\text{m}^2\text{sr}}
    ```
2. Determine the radiant flux delivered to the film.

    Assume that the camera aperture is parallel to the source surface and centered along the direction of the surface normal. $`D=0.5\text{ cm}`$. The flux $`\Phi_{\text{cam}}`$ is well approximated by multiplying the intensity in the forward direction by the solid angle intercepted by the camera lens. The solid angle subtended by the lens is 
    ```math
    \Omega_{\text{lens}}=2\pi(1-\cos\theta)\approx\pi\theta^2=\pi\frac{(0.25 cm)^2}{(100 cm)^2}=1.96\cdot10^{-5} \text{ sr}
    ```
    and thus
    ```math
    \Phi_{\text{cam}}=\iint I(0) \mathrm{d}\Omega=I(0)\Omega_{\text{lens}}=7.96 \text{W}/\text{sr}1.96\cdot10^{-5} \text{ sr}=1.56\cdot10^{-4}\text{ W}
    ```
3. Determine the irradiance at the film.
    ```math
    E_e=\frac{\Phi_{\text{cam}}}{A{\text{image}}}=\frac{\Phi_{\text{cam}}}{\left|M^2\right|A}
    ```
    where $`M`$ is the magnification. Note that each image dimension is decreased from the source dimension by the magnification,
    ```math
    \left|M\right|=\frac{s'}{s}=\frac{f}{s-f}=\frac{4}{100-4}=0.0417
    ```
    and
    ```math
    E_e=\frac{1.56\cdot10^{-4}\text{ W}}{(0.0417)^2(25\text {cm})^2}=35.9\text{ W}/\text{m}^2
    ```

Given radinace of a radiating body, calculation of the power on a detector:
```math
P=\frac{W_\lambda \times \text{detector area}\times \text{lens area}}{\pi \times\left(\text{focal length}\right)^2}=\frac{\pi N_\text{target}}{4\cdot \left(\frac{f}{D}\right)^2\left(1+M\right)^2}
```

### Planck constants
$`c_1=3.74\cdot10^4 \frac{\text{W}\text{ µm}^2}{\text{cm}^2}`$

$`c_2=1.44\cdot10^4 \frac{\text{W}}{\text{cm}^2\text{ µm}}`$

$`c_3=1.88\cdot10^{23} \frac{\text{µm}^3}{\text{sec}\text{ cm}^2}`$

### $`\cos^4(\theta)
see `cosine 4th power law.ggb`.