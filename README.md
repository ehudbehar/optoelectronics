# Optoelectronics

## Conversion
```math
1 \text{µm}^2=1\cdot10^{-8}\text{ cm}^2=1\cdot10^{-6}\text{ mm}^2=1\cdot10^{-12}\text{ m}^2
```