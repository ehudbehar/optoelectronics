### Index ellipdoid
Lookup table for the index $`I`$ that represents the pair of indices $`i,j`$.

| j/i  | 1 | 2 | 3 |
| ------ | ------ | ------ | ------ |
| 1 | 1 | 6 | 5 |
| 2 | 6 | 2 | 4 |
| 3 | 5 | 4 | 3 |

 ```math
\begin{aligned}
\sum_i \sum_j \eta_{ij}x_i x_j &= \left(\frac{1}{n_x^2}+r_{11k}E_k\right)x^2+\left(\frac{1}{n_y^2}+r_{22k}E_k\right)y^2+\left(\frac{1}{n_z^2}+r_{33k}E_k\right)z^2 \\
    &+ 2x\ y\  r_{12k} E_k+ 2y\ z\  r_{23k} E_k +2x\ z\  r_{13k}E_k\\
    &= 1
\end{aligned}
 ```
 For each direction of $`k`$: $`k\in\left[ 1,2,3\right]`$.

 
 - Question 5 from chapter 15.

 ```math
 n=\frac{1}{\sqrt{\frac{1}{n_o^2}+r_{13}E_z-r_{22}E_y}} \approx  n_o-\frac{1}{2}n_o^3 r_{13}E_z+\frac{1}{2}n_o^3 r_{22}E_y
 ```

 Given $`\eta(E)=\frac{1}{n^2}=x^2(\eta_{xx})+y^2(\eta_{yy}+)`$

 ```math

\left(
\begin{array}{ccc}
 \eta_{xx} & 0 & 0 \\
 0 & \eta_{yy} & \eta_{zy} \\
 0 & \eta_{zy} & \eta _{zz} \\
\end{array}
\right)\Rightarrow v_1=\{1,0,0\};\ v_2=\left\{0,-\frac{\sqrt{\eta _{yy}^2-2 \eta _{yy} \eta _{zz}+4 \eta _{zy}^2+\eta _{zz}^2}-\eta _{yy}+\eta _{zz}}{2 \eta _{zy}},1\right\} ;\ v_3=\left\{0,-\frac{-\sqrt{\eta _{yy}^2-2 \eta _{yy} \eta _{zz}+4 \eta _{zy}^2+\eta _{zz}^2}-\eta _{yy}+\eta _{zz}}{2 \eta _{zy}},1\right\}
 ```
